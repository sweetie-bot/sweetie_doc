# Sweetie Bot Project

### This is main documentation repositary [here [RUS]](https://gitlab.com/sweetie-bot/sweetie_doc/wikis/home).
### Main software repository located [here](https://gitlab.com/sweetiebot/sweetie_bot).

![](https://gitlab.com/sweetie-bot/sweetie_doc/raw/master/logo-sweetie-bot.png)
